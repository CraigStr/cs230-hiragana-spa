<!--
#############################################
#############################################
#############################################

    README.MD present in front-end.tar.gz

#############################################
#############################################
#############################################
-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>
        Hiragana
    </title>
    <link rel="stylesheet" href="css/materialize/css/materialize.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

    <?php
    include 'session.php';
    ?>

</head>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">Logo</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="#" id="userStats" data-target="userScores" class="modal-trigger">Your Scores</a></li>
            <li><a href="#" id="quizToggle">QUIZ</a></li>
            <li><a id="logindrop" href="#">Login</a></li>
            <li><a id="logout" href="#">Logout</a></li>
        </ul>
    </div>
</nav>
<div class="row">
    <div id="login-form" class="webui-popover-content">
        <div>
            <ul class="tabs center-align">
                <li class="tab col s3"><a href="#login" class="active">Login</a></li>
                <li class="tab col s3"><a href="#register">Register</a></li>
            </ul>
        </div>
        <div id="login" class="col l s12">
            <h4 class="center-align">Login</h4>
            <form action="login.php" id="loginForm">
                <input type="text" name="username" placeholder="Username" class="box"/><br/><br/>
                <input type="password" name="password" placeholder="Password" class="box"/><br/><br/>
                <button class="btn waves-effect waves-light" type="submit" name="action">Login
                    <i class="material-icons right">send</i>
                </button>
            </form>
        </div>
        <div id="register" class="col s12">
            <h4 class="center-align">Register</h4>
            <form id="registerForm" action="register.php" method="post">
                <input type="text" name="username" placeholder="Username" class="box"/><br/><br/>
                <input type="password" name="password" placeholder="Password" class="box"/><br/><br/>
                <input type="password" name="confirmPassword" placeholder="Password" class="box"/><br/><br/>
                <button class="btn waves-effect waves-light" type="submit" name="action">Register
                    <i class="material-icons right">send</i>
                </button>
            </form>
        </div>
    </div>
</div>

<div class="row" id="soundHiragana">
    <?php
    $strJsonFileContents = file_get_contents("romaji.json");
    $array = json_decode($strJsonFileContents, true);

    $directory = 'resources/images/';
    $files = array_diff(scandir($directory), array('..', '.'));
    foreach ($files as $file) {
        $bn = basename($file, ".png");
        echo '<div class="col l1 m2 s5 center-align"><img class="playSound responsive-img" src="resources/images/' . $file . '" type="image/png" alt="' . $file . '">' . $array[$bn] . '</div>';
    }
    ?>

    <button data-target="creditResources" class="btn modal-trigger">Resoures Used</button>

    <div id="creditResources" class="modal">
        <div class="modal-content">
            <ul class="collection with-header">
                <li class="collection-header"><h4>Resources Used</h4></li>
                <li class="collection-item avatar">
                    <img src="resources/creditImgs/jQuery.gif" class="circle" alt="">
                    jQuery
                    <a href="https://jquery.com/" class="secondary-content"><i
                                class="material-icons">send</i></a>
                </li>
                <li class="collection-item avatar">
                    <img src="resources/creditImgs/materializecss.png" class="circle" alt="">
                    MaterializeCSS
                    <a href="https://materializecss.com/" class="secondary-content"><i
                                class="material-icons">send</i></a>
                </li>
                <li class="collection-item avatar">
                    <img src="resources/creditImgs/github.svg" class="circle" alt="">
                    jQuery WebUI Popover
                    <a href="https://github.com/sandywalker/webui-popover"
                       class="secondary-content"><i class="material-icons">send</i></a>
                </li>
                <li class="collection-item avatar">
                    <img src="resources/creditImgs/guidetojapanese.png" class="circle" alt="">
                    Kana Sounds
                    <a href="http://www.guidetojapanese.org/learn/complete/hiragana"
                       class="secondary-content"><i class="material-icons">send</i></a>
                </li>
                <li class="collection-item avatar">
                    <img src="resources/creditImgs/commonswiki.png" class="circle" alt="">
                    Kana Stroke Order Images/Gifs
                    <a href="https://commons.wikimedia.org/wiki/Category:Hiragana_stroke_order_(animated_image_set)"
                       class="secondary-content"><i class="material-icons">send</i></a>
                </li>
            </ul>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>


    <div id="userScores" class="modal bottom-sheet">
        <div class="modal-content score-content">
        </div>
    </div>

</div>

<div class="row">
    <div id="quizHiragana">
        <?php
        $strJsonFileContents = file_get_contents("romaji.json");
        $array = json_decode($strJsonFileContents, true);

        $directory = 'resources/images/';
        $files = array_diff(scandir($directory), array('..', '.'));
        foreach ($files as $file) {
            $bn = basename($file, ".png");
            echo '
                <div class="col l1">
                    <label>
                        <input type="checkbox" class="quizTemplate"/>
                        <span>
                            <div class="center-align"><img class="responsive-img" src="resources/images/' . $file . '" type="image/png" alt="' . $file . '">' . $array[$bn] . '</div>
                        </span>
                    </label>
                </div>';
        };
        ?>
        <div class="col l2 offset-l10">
            <button class="btn-large waves-effect waves-light" id="quizSelectElements">Select
                <i class="material-icons right">send</i>
            </button>
        </div>
    </div>
</div>

<!-- Element Showed -->
<a id="menu" class="waves-effect waves-light btn btn-floating transparent"><i class="material-icons">menu</i></a>

<!-- Tap Target Structure -->
<div class="tap-target" data-target="menu">
    <div class="tap-target-content">
        <h5 id="tap-target-head">Oops!</h5>
        <p id="tap-target-text">Incorrect Login Details</p>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script src="css/materialize/js/materialize.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.js"></script>
<script src="js/js.js"></script>
</body>
</html>