<?php
$username = $_POST['username'];
//$results = json_decode($_POST['results']);
if (isset($_POST['results'])) {
    $results = json_decode(json_encode($_POST['results']));
} else {
    echo "NO RESULTS";
    return;
}

$file = "kanaRecognition.json";

$strJsonFileContents = file_get_contents($file);
$array = json_decode($strJsonFileContents, true);

//if (is_array($results)) {
foreach ($results as $key => $value) {
//        echo $key . ":" . $value;
    $array['users'][$username][$key]['attempts']++;
    if ($value === 'success') {
        $array['users'][$username][$key]['correct']++;
        echo 'Correct';
    }
}
//}

//echo json_encode($array);

$json = json_encode($array);

file_put_contents($file, $json);