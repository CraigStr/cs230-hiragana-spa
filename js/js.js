/**
 Cosmetics
 **/
$(".playSound").click(function () {
    const path = "resources/audio/" + baseName($(this).attr('src')) + ".mp3";
    const audio = new Audio(path);
    audio.type = 'audio/mp3';
    const play = audio.play();
});

$(".playSound").mouseenter(function () {
    $(this).attr({src: 'resources/gifs/' + baseName(this.getAttribute('src')) + '.gif'})
});

$(".playSound").mouseleave(function () {
    $(this).attr({src: 'resources/images/' + baseName(this.getAttribute('src')) + '.png'})
});

function baseName(str) {
    let base = String(str).substring(str.lastIndexOf('/') + 1);
    if (base.lastIndexOf(".") !== -1)
        base = base.substring(0, base.lastIndexOf("."));
    return base;
}

/**
 Login System
 **/
let username = getLoginUser();

$('#logindrop').webuiPopover({url: '#login-form'});

$(document).ready(function () {
    $('.tabs').tabs();
    $('.tap-target').tapTarget();
    $('select').formSelect();
    $('.modal').modal();

    $('#logindrop').css('width', $('#logout').css('width'));
    $('#quizHiragana').hide();

    if (username !== null) successfulLogin();
    else successfulLogout();
});

function getLoginUser() {
    $.getJSON("vars.php", function (response) {
        username = response.session;
    });
}

$("#loginForm").submit(function (event) {
    event.preventDefault();
    const post_url = $(this).attr("action");
    const form_data = $("#loginForm").serialize();

    $.post(post_url, form_data, function (response, status) {
        if (response === "Your Login Name or Password is invalid") {
            $('.tap-target').removeClass('green');
            $('.tap-target').addClass('red');
            $('#tap-target-head').html('Oops!');
            $('#tap-target-text').html('Incorrect Login Details');
        } else {
            $('.tap-target').removeClass('red');
            $('.tap-target').addClass('green');
            $('#tap-target-head').html('Correct');
            $('#tap-target-text').html('Logged In');
            $('#logindrop').webuiPopover('hide');
            successfulLogin();
        }
        getLoginUser();
        $('.tap-target').tapTarget('open');
        setTimeout(function () {
            $('.tap-target').tapTarget('close');
        }, 1000);
    });
});

$("#registerForm").submit(function (event) {
    event.preventDefault();
    const post_url = $(this).attr("action");
    const form_data = $('#registerForm').serialize();

    $.post(post_url, form_data, function (response, status) {
        if (response !== "Registered!") {
            $('.tap-target').removeClass('green');
            $('.tap-target').addClass('red');
            $('#tap-target-head').html('Oops!');
            if (response === "Username already Exists") {
                $('#tap-target-text').html('Username already Exists');
            } else {
                $('#tap-target-text').html('Passwords do not match!');
            }
        } else {
            $('.tap-target').removeClass('red');
            $('.tap-target').addClass('green');
            $('#tap-target-head').html('Registered!');
            $('#tap-target-text').html('Please Log in!');
            $('#logindrop').webuiPopover('hide');
        }
        getLoginUser();
        $('.tap-target').tapTarget('open');
        setTimeout(function () {
            $('.tap-target').tapTarget('close');
        }, 1000);
    });
});

$('#logout').click(function () {
    $.post('logout.php', function (response, status) {
        username = null;
        successfulLogout();
    })
});

function successfulLogin() {
    $('#logindrop').hide();
    $('#logout').show();
    $('#userStats').show();
    $('.brand-logo').html("Logged In");
    if (quizStarted) {
        setTimeout(function () {
            $.ajax({
                url: updateResults(),
                success: resetQuiz()
            })
        }, 500);
    }
}

function successfulLogout() {
    $('#logindrop').show();
    $('#logout').hide();
    $('#userStats').hide();
    $('.brand-logo').html("Not Logged In");
}

/**
 User Statistics
 **/

$('#userStats').click(function () {
    $('.score-content').html("");
    $.ajax({
        dataType: "json",
        url: "kanaRecognition.json",
        mimeType: "application/json",
        success: function (data) {
            $.each(data['users'][username], function (i, item) {
                $('.score-content').append('<p>' + i + syntaxHighlight(item) + '</p>');
            })
        }
    });
});

function syntaxHighlight(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, null, '\t');
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

/**
 Quiz
 **/

$('#quizToggle').click(function () {
    $('#quizHiragana').toggle();
    $('#soundHiragana').toggle();
});

$('#quizSelectElements').click(function () {
    if ($('input:checked').length >= 10) {
        const quizQuestions = [];
        $('input:checked').each(function () {
            quizQuestions.push(baseName($(this).parent().find('div').find('img').attr('src')));
        });
        $('#quizHiragana').hide();
        $('#quizToggle').hide();
        startQuiz(quizQuestions);
    } else {
        $('.tap-target').removeClass('green');
        $('.tap-target').addClass('red');
        $('#tap-target-head').html('You need to select at least 10 characters');
        $('#tap-target-text').html('');
        $('.tap-target').tapTarget('open');
        setTimeout(function () {
            $('.tap-target').tapTarget('close');
        }, 1000);
    }
});

let quizStarted = false;

const keys = [
    "あ", "い", "う", "え", "お", "か", "き", "く", "け", "こ", "さ", "し",
    "す", "せ", "そ", "た", "ち", "つ", "て", "と", "な", "に", "ぬ", "ね",
    "の", "は", "ひ", "ふ", "へ", "ほ", "ま", "み", "む", "め", "も", "や",
    "ゆ", "よ", "ら", "り", "る", "れ", "ろ", "わ", "を", "ん"
];
const values = [
    "a", "i", "u", "e", "o", "ka", "ki", "ku", "ke", "ko", "sa", "shi",
    "su", "se", "so", "ta", "chi", "tsu", "te", "to", "na", "ni", "nu", "ne",
    "no", "ha", "hi", "fu", "he", "ho", "ma", "mi", "mu", "me", "mo", "ya",
    "yu", "yo", "ra", "ri", "ru", "re", "ro", "wa", "o", "n"
];

function startQuiz(quizQuestions) {
    quizStarted = true;
    quizQuestions = shuffle(quizQuestions).slice(0, 10);
    $('.row:has(#quizHiragana)').after('<div class="container"><div class="row" id="questionImgs"></div><form id="quizForm"></form></div></div>');

    let romaji = {};
    $.ajax({
        dataType: "json",
        url: "romajiUTF16.json",
        mimeType: "application/json",
        success: function (result) {
            romaji = result;
        }
    });
    /**
     Kana to Romaji
     **/
    $.each(quizQuestions, function (index, value) {
        let ansOptions = shuffle([values[keys.indexOf(value)],
            values[Math.floor(Math.random() * values.length)],
            values[Math.floor(Math.random() * values.length)],
            values[Math.floor(Math.random() * values.length)]]);
        $('#quizForm').append('<div class="center-align"><img class="responsive-img" src="/resources/images/' + value + '.png" type="image/png" alt="' + value + '">' +
            '    <div class="input-field col s12">' +
            '       <select>' +
            '           <option value="" disabled selected>Choose your answer</option>' +
            '           <option value="1">' + ansOptions[0] + '</option>' +
            '           <option value="2">' + ansOptions[1] + '</option>' +
            '           <option value="3">' + ansOptions[2] + '</option>' +
            '           <option value="4">' + ansOptions[3] + '</option>' +
            '       </select>' +
            '       <label></label>' +
            '    </div>' +
            '</div>'
        );
        return index < 4;
    });
    /**
     Romaji to Kana
     **/
    $.each(quizQuestions.reverse(), function (index, value) {
        let ansOptions = shuffle([value,
            keys[Math.floor(Math.random() * keys.length)],
            keys[Math.floor(Math.random() * keys.length)],
            keys[Math.floor(Math.random() * keys.length)]]);
        $('#quizForm').append('<div class="center-align"><h1>' + values[keys.indexOf(value)] + '</h1>' +
            '    <div class="input-field col s12">' +
            '       <select>' +
            '           <option value="" disabled selected>Choose your answer</option>' +
            '           <option value="1">' + ansOptions[0] + '</option>' +
            '           <option value="2">' + ansOptions[1] + '</option>' +
            '           <option value="3">' + ansOptions[2] + '</option>' +
            '           <option value="4">' + ansOptions[3] + '</option>' +
            '       </select>' +
            '       <label></label>' +
            '    </div>' +
            '</div>'
        );
        return index < 4;
    });
    $('select').formSelect();
    $('#quizForm').append(
        '<button class="btn-large waves-effect waves-light right" id="submitQuizAnswers">' +
        '   <i class="material-icons right">send</i> Submit Answers' +
        '</button>');
}

function shuffle(a) {
    let j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}


let quizResults = {};
$(document).on('submit', "#quizForm", function (event) {
    event.stopPropagation();
    if (username === null) {
        $('.tap-target').removeClass('red');
        $('.tap-target').addClass('green');
        $('#tap-target-head').html('Log in to save progress?');
        $('#tap-target-text').html('Simply leave the quiz to continue without saving,\nLogin to save Progress');
        $('.tap-target').tapTarget('open');
        $('#logindrop').webuiPopover('show');
    } else {
        $('.tap-target').removeClass('red');
        $('.tap-target').addClass('green');
        $('#tap-target-head').html('Progress Saved');
        $('#tap-target-text').html('Already logged in');
        $('.tap-target').tapTarget('open');
    }
    setTimeout(function () {
        $('.tap-target').tapTarget('close');
    }, 5000);

    $.ajax({
        url: getResults(),
        success: function () {
            if (username !== null) {
                updateResults();
            }
        }
    })
});

function getResults() {
    let score = 0;
    /**

     **/
    $('#quizForm').find('input').each(function (index, value) {
        let kana = baseName($(this).parent().parent().prev().attr('src'));
        let answer = $(this).val();
        $.post('returnJSONvalue.php', {char: kana}, function (data) {
            if (data.toLowerCase() === answer.toLowerCase()) {
                score++;
                quizResults[kana] = "success";
                $('.tap-target').removeClass('red');
                $('.tap-target').addClass('green');
                $('#tap-target-head').html('SCORE: ' + score * 10 + "%");
                $('#tap-target-text').html(username === null ? 'Login to save progress!' : 'Progress Recorded');
                $('.tap-target').tapTarget('open');
                setTimeout(function () {
                    $('.tap-target').tapTarget('close');
                }, 5000);
            } else {
                quizResults[kana] = "fail";
            }
        });
        return index < 4;
    });
    $($('#quizForm').find('input').get().reverse()).each(function (index, value) {
            let romaji = baseName($(this).parent().parent().prev().text());
            let answer = $(this).val();
            if (values[keys.indexOf(answer)] === romaji) {
                score++;
                quizResults[answer] = "success";
                $('.tap-target').removeClass('red');
                $('.tap-target').addClass('green');
                $('#tap-target-head').html('SCORE: ' + score * 10 + "%");
                $('#tap-target-text').html(username === null ? 'Login to save progress!' : 'Progress Recorded');
                $('.tap-target').tapTarget('open');
                setTimeout(function () {
                    $('.tap-target').tapTarget('close');
                }, 5000);
            } else {
                quizResults[keys[values.indexOf(romaji)]] = "fail";
            }
            return index < 4;
        }
    );
    $('.container').append(
        '<button class="btn-large waves-effect waves-light orange" id="stopQuiz">' +
        '   <i class="material-icons right">send</i> Stop Quiz' +
        '</button>');
}

function updateResults() {
    console.log(quizResults);
    $.post('updateJSON.php', {username: username, results: quizResults}, function (data) {
    })
}

function resetQuiz() {
    $('#quizHiragana').hide();
    $('#soundHiragana').show();
    $('#quizToggle').show();
    $('.container').remove();
}

$(document).on('click', '#stopQuiz', function (event) {
    quizResults = {};
    quizStarted = false;
    resetQuiz();
});